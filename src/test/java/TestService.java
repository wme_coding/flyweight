import org.junit.Test;

public class TestService {
    private String shapes[] = { "Triangular", "Squared", "Rectangle", "Circled"};

    @Test
    public void testService(){
        for(int i=0; i < 20; ++i) {
            RoofObstacle roofObstacle = (RoofObstacle) ObstacleFactory.getRoofObstacle(getRandomColor());
            roofObstacle.draw();
        }
    }

    private String getRandomColor() {
        return shapes[(int)(Math.random()*shapes.length)];
    }
}
