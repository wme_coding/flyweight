import java.util.HashMap;
import java.util.Map;

public class ObstacleFactory {
    private static final Map<String, Obstacle> obstacleMap = new HashMap<>();

    public static Obstacle getRoofObstacle(String shape){
        RoofObstacle res = (RoofObstacle) obstacleMap.get(shape);

        if(res == null) {
            res = new RoofObstacle(shape);
            obstacleMap.put(shape, res);
            System.out.println("Creating Roof Obstacle of shape : " + shape);
        }
        return res;
    }
}
