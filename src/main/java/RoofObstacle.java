public class RoofObstacle implements Obstacle {
    private String shape;

    public RoofObstacle(String shape) {
        this.shape = shape;
    }


    @Override
    public void draw() {
        System.out.println("Drawing " + shape + " Roof Obstacle");
    }
}
